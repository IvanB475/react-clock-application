import logo from './logo.svg';
import './App.css';
import { InputForm } from './inputComponent';
import { useState } from 'react';

function App() {
  const [firstName, setFirstName] = useState("");

  const handleChange = (text) => {
    setFirstName(text);
  }
  return (
    <div className="App">
      <InputForm onChange={handleChange}></InputForm>
    </div>
  );
}

export default App;
