import React, {useState} from "react";
import InputFormComponent from "./inputForm";

export default function InputForm(props) {
    const [ firstName, setFirstName ] = useState("");
    const [ message, setMessage ] = useState("");

    const handleMessage = () => {
        const time = new Date();
        const hours = time.getHours();
        if(hours > 7 && hours < 12) {
            setMessage("Good morning, ");
        } else if( hours > 11 && hours < 18) {
            setMessage("Good afternoon, ")
        } else if( hours > 17 && hours < 24) {
            setMessage("Good evening, ")
        } else {
            setMessage("Good night, ")
        }
    }

    const handleChange = (event) => {
        setFirstName(event.target.value);
        handleMessage();
    }


    return(
        <InputFormComponent 
        handleChange = {handleChange}
        firstName = {firstName}
        message = {message}/>
    );
}