import React from "react";


export default class InputFormComponent extends React.Component {
    render() {
        return (
            <div onChange={this.props.handleChange}>
                <h2>{this.props.message} {this.props.firstName} </h2>
                <p>{this.props.time}</p>
                <input type="text" placeholder="enter your name" value = {this.props.firstName}/>
            </div>
        )
    }
}